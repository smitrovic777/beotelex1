package com.strahnja.beotel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Strahinja
 */
public class Repository {

    protected Connection conn;

    public Repository() {
        conn = MyConnection.getConnection();
    }

    public List<Customer> getAllCustomers() throws SQLException {
        List<Customer> customers = new ArrayList<Customer>();
        Statement st = conn.createStatement();
        st.execute("select * from customer_view");
        ResultSet rs = st.getResultSet();
        while (rs.next()) {
            Customer c = new Customer();
            Package p = new Package();
            c.setId(rs.getInt("idcustomer"));
            c.setFirst_name(rs.getString("firstname"));
            c.setLast_name(rs.getString("lastname"));
            c.setPaid_until(rs.getDate("paid_until"));
            c.setTel_num(rs.getInt("tel_num"));
            p.setName(rs.getString("package"));
            p.setPrice_for_one_month(rs.getDouble("price_for_one_month"));
            p.setPrice_for_three_months(rs.getDouble("price_for_three_months"));
            c.setCus_package(p);
            customers.add(c);
        }
        return customers;
    }

}

package com.strahnja.beotel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Date;

/**
 *
 * @author Strahinja
 */
public class Customer extends Person {

    protected int id;
    protected Date paid_until;
    protected Package cus_package;

    /**
     * @return the paid_until
     */
    public Date getPaid_until() {
        return paid_until;
    }

    /**
     * @param paid_until the paid_until to set
     */
    public void setPaid_until(Date paid_until) {
        this.paid_until = paid_until;
    }

    /**
     * @return the cus_package
     */
    public Package getCus_package() {
        return cus_package;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @param cus_package the cus_package to set
     */
    public void setCus_package(Package cus_package) {
        this.cus_package = cus_package;
    }

    public String toString() {
        return "Firstname: " + getFirst_name() + " Lastname: " + getLast_name() + " Package: " + getCus_package().getName();
    }
}

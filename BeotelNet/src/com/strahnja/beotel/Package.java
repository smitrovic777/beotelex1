package com.strahnja.beotel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Strahinja
 */
public class Package {
    
    protected String name;
    protected double price_for_one_month;
    protected double price_for_three_months;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the price_for_one_month
     */
    public double getPrice_for_one_month() {
        return price_for_one_month;
    }

    /**
     * @param price_for_one_month the price_for_one_month to set
     */
    public void setPrice_for_one_month(double price_for_one_month) {
        this.price_for_one_month = price_for_one_month;
    }

    /**
     * @return the price_for_three_months
     */
    public double getPrice_for_three_months() {
        return price_for_three_months;
    }

    /**
     * @param price_for_three_months the price_for_three_months to set
     */
    public void setPrice_for_three_months(double price_for_three_months) {
        this.price_for_three_months = price_for_three_months;
    }
    
}

package com.strahnja.beotel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Strahinja
 */
public class Logic {

    public static double all_income;

    public static void test_All_Customers_Pay_Monthly() {
        try {
            Repository repo = new Repository();
            List<Customer> customers;
            customers = repo.getAllCustomers();
            if (all_income != 0) {
                all_income = 0;
            }
            for (Customer c : customers) {
                all_Customers_Pay_Monthly(c);
            }
            System.out.println("Expected income:" + all_income);
        } catch (SQLException ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void test_All_Customers_Pay_for_Three_Months() {
        try {
            Repository repo = new Repository();
            List<Customer> customers;
            customers = repo.getAllCustomers();
            if (all_income != 0) {
                all_income = 0;
            }
            for (Customer c : customers) {
                all_Customers_Pay_for_Three_Months(c);
            }
            System.out.println("Expected income:" + all_income);
        } catch (SQLException ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void test_only_Hosting_Cutomers_Pay_In_Quater() {
        try {
            Repository repo = new Repository();
            List<Customer> customers;
            customers = repo.getAllCustomers();
            if (all_income != 0) {
                all_income = 0;
            }
            for (Customer c : customers) {
                only_Hosting_Cutomers_Pay_In_Quater(c);
            }
            System.out.println("Expected income:" + all_income);
        } catch (SQLException ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void all_Customers_Pay_Monthly(Customer c) throws ParseException {
        System.out.println("Monthly subscription: ");
        double income_per_cus;
        Date customer_date = c.getPaid_until();
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        String date_format = format.format(customer_date);
        String[] date = date_format.split("\\.");
        Date start = format.parse(date[0] + ".12.2017");
        long left = start.getTime() - customer_date.getTime();
        long days = TimeUnit.MILLISECONDS.toDays(left);
        int months = (int) (days / 30);
        income_per_cus = c.getCus_package().getPrice_for_one_month() * months;
        all_income = all_income + income_per_cus;
        System.out.println(c.toString() + " Package cost for one month: " + c.getCus_package().getPrice_for_one_month() + " Paid Until: " + date_format + " Months left until end of 2017: " + months);
        System.out.println("Income from customer: "+income_per_cus);

    }

    public static void all_Customers_Pay_for_Three_Months(Customer c) throws ParseException {
        System.out.println("Subscription for three months: ");
        double income_per_cus = c.getCus_package().getPrice_for_three_months();
        Date customer_date = c.getPaid_until();
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        String date_format = format.format(customer_date);
        String[] date = date_format.split("\\.");
        Date start = format.parse(date[0] + ".12.2017");
        long left = start.getTime() - customer_date.getTime();;
        long days = TimeUnit.MILLISECONDS.toDays(left);
        int quater = 0;
        int months = (int) (days / 30);
        if (months != -1 || months == 0) {
            quater = months / 3;
        }
        income_per_cus = quater * c.getCus_package().getPrice_for_three_months();
        all_income = all_income + income_per_cus;
        System.out.println(c.toString() + " Package cost for one quater: " + c.getCus_package().getPrice_for_three_months() + " Paid Until: " + date_format + " Quaters left until end of 2017: " + quater);
        System.out.println("Income from customer: "+income_per_cus);
    }

    public static void only_Hosting_Cutomers_Pay_In_Quater(Customer c) throws ParseException {

        System.out.println("Subscription where only Hosting customers pay in quater, the rest pay montly: ");
        double income_per_cus = c.getCus_package().getPrice_for_three_months();
        Date customer_date = c.getPaid_until();
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        String date_format = format.format(customer_date);
        String[] date = date_format.split("\\.");
        Date start = format.parse(date[0] + ".12.2017");
        long left = start.getTime() - customer_date.getTime();
        long days = TimeUnit.MILLISECONDS.toDays(left);
        int quater = 0;
        int months = (int) (days / 30);
        if (c.getCus_package().getName().contains("Hosting")) {
            if (months != -1 || months == 0) {
                quater = months / 3;
            }
            income_per_cus = quater * c.getCus_package().getPrice_for_three_months();
            all_income = all_income + income_per_cus;
            System.out.println(c.toString() + " Package cost for one quater: " + c.getCus_package().getPrice_for_three_months() + " Paid Until: " + date_format + "  Quaters left until end of 2017: " + quater);
            System.out.println("Income from customer: "+income_per_cus);
        } else {
            income_per_cus = c.getCus_package().getPrice_for_one_month() * months;
            all_income = all_income + income_per_cus;
            System.out.println(c.toString() + " Package cost for one month: " + c.getCus_package().getPrice_for_one_month() + " Paid Until: " + date_format + " Months left until end of 2017: " + months);
            System.out.println("Income from customer: "+income_per_cus);
        }

    }
}

package com.strahnja.beotel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Strahinja
 */
public abstract class Person {
    
    protected String first_name;
    protected String last_name;
    protected int tel_num;

    /**
     * @return the first_name
     */
    public String getFirst_name() {
        return first_name;
    }

    /**
     * @param first_name the first_name to set
     */
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    /**
     * @return the last_name
     */
    public String getLast_name() {
        return last_name;
    }

    /**
     * @param last_name the last_name to set
     */
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
    
    public String toString(){
        return "Firstname: "+this.first_name+" Lastname: "+this.last_name;
    }

    /**
     * @return the tel_num
     */
    public int getTel_num() {
        return tel_num;
    }

    /**
     * @param tel_num the tel_num to set
     */
    public void setTel_num(int tel_num) {
        this.tel_num = tel_num;
    }
    
    
}

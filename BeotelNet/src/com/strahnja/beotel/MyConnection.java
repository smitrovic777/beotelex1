package com.strahnja.beotel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Strahinja
 */
public class MyConnection {
    
    private static Connection instance;
    
    private MyConnection(){
    
    }
    
    public static synchronized Connection getConnection(){
        
        if(instance==null){
            try {
                instance = DriverManager.getConnection("jdbc:mysql://localhost:3306/beotel","root","");
            } catch (SQLException ex) {
                Logger.getLogger(MyConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return instance;
    }
    
    
}

package test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.strahnja.beotel.Logic;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Strahinja
 */
public class BeoTelTest {
    
    public BeoTelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     /**
     * Test of test_All_Customers_Pay_Monthly method, of class TestJUnit.
     */
    @Test
    public void testTest_All_Customers_Pay_Monthly() {
        System.out.println("");
        System.out.println("");
        System.out.println("test_All_Customers_Pay_Monthly");
        Logic.test_All_Customers_Pay_Monthly();
        assertEquals((Double) Logic.all_income, (Double) 88500.0);
        System.out.println("");
        System.out.println("");
    }

    /**
     * Test of test_All_Customers_Pay_for_Three_Months method, of class
     * TestJUnit.
     */
    @Test
    public void testTest_All_Customers_Pay_for_Three_Months() {
        System.out.println("");
        System.out.println("");
        System.out.println("test_All_Customers_Pay_for_Three_Months");
        Logic.test_All_Customers_Pay_for_Three_Months();
        assertEquals((Double) Logic.all_income, (Double) 71500.0);
        System.out.println("");
        System.out.println("");
    }

    /**
     * Test of test_only_Hosting_Cutomers_Pay_In_Quater method, of class
     * TestJUnit.
     */
    
    @Test
    public void testTest_only_Hosting_Cutomers_Pay_In_Quater() {
        System.out.println("");
        System.out.println("");
        System.out.println("test_only_Hosting_Cutomers_Pay_In_Quater");
        Logic.test_only_Hosting_Cutomers_Pay_In_Quater();
        assertEquals((Double) Logic.all_income, (Double) 87700.0);
        System.out.println("");
        System.out.println("");
    }
}
